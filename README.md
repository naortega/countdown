# CountDown.sh
A simple script to countdown from a certain number to zero. I made it so that
I can easily delay the playing time of songs by a few seconds before I play it
on the guitar.

```
USAGE:
	countdown.sh <sec>

OPTIONS:
	<sec>      number to countdown from [default: 5]

EXAMPLE:
	Countdown from 7 and then play Scott Stapp's "Last Hallelujah"
		./countdown 7 ; mpv 12_-_Last_Hallelujah.flac
```

## License

This project is licensed under the terms & conditions of the ZLib license.
